/**
 * Project Name: nettyStudy
 * File Name: Client
 * Package Name: com.hmz.netty
 * Date: 2020/8/8 10:57
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

import com.hmz.tank.Dir;
import com.hmz.tank.Group;
import com.hmz.tank.Tank;
import com.hmz.tank.TankFrame;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.ReferenceCountUtil;

import java.util.UUID;

public class Client {

    public static final Client INSTANCE = new Client();

    public Channel channel;

    private Client(){}

    public void connect(){
        EventLoopGroup group = new NioEventLoopGroup(1);

        try {
            Bootstrap b = new Bootstrap();
            ChannelFuture f = b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new TankJoinMsgEncoder())
                                    .addLast(new TankJoinMsgDecoder())
                                    .addLast(new ClientHandler());
                        }
                    })
                    .connect("localhost", 8888);
            f.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if(!future.isSuccess()) {
                        System.out.println(" not connected!");
                    } else {
                        System.out.println(" connected!");
                        channel = f.channel();
                    }
                }
            });
            f.sync();
            System.out.println("....");
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }


//    public void send(String text) {
//        ByteBuf buf = Unpooled.copiedBuffer(text.getBytes());
//        channel.writeAndFlush(buf);
//    }

    public void send(Msg msg) {
        channel.writeAndFlush(msg);
    }

    public void closeContext() {
        ByteBuf buf = Unpooled.copiedBuffer("_bye_".getBytes());
        channel.writeAndFlush(buf);
    }
}

class ClientHandler extends SimpleChannelInboundHandler<TankJoinMsg> {

//    ClientFrame clientFrame = new ClientFrame();
//    TextArea textArea = clientFrame.ta;
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//
        ctx.writeAndFlush(new TankJoinMsg(TankFrame.INSTANCE.getMainTank()));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TankJoinMsg msg) throws Exception {
//        if (msg.id.equals(TankFrame.INSTANCE.getMainTank().getId())) {
//            return;
//        }
//        System.out.println(msg);
//        Tank t = new Tank(msg);
//        TankFrame.INSTANCE.addTank(t);
//        ctx.writeAndFlush(new TankJoinMsg(TankFrame.INSTANCE.getMainTank()));
        msg.handle();
    }

    /*@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        *//*ByteBuf buf = null;
        try {
            buf = (ByteBuf)msg;
            byte[] bytes = new byte[buf.readableBytes()];
            buf.getBytes(buf.readerIndex(), bytes);
            String msgAccepted = new String(bytes);

        } finally {
            if (buf != null) {
                ReferenceCountUtil.release(buf);
            }
        }*//*
        System.out.println("客户端收到回写的 : " + msg);
    }*/

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
