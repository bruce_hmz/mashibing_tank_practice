/**
 * Project Name: tank_practice
 * File Name: Test
 * Package Name: com.hmz.tank.net
 * Date: 2020/8/10 22:56
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

import com.hmz.tank.Dir;

public class Test {
    public static void main(String[] args) {
        Dir dir = Dir.DOWN;
        System.out.println(dir.ordinal());
    }
}
