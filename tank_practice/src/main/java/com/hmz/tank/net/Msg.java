/**
 * Project Name: tank_practice
 * File Name: Msg
 * Package Name: com.hmz.tank.net
 * Date: 2020/8/15 12:00
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

public abstract class Msg {
    public abstract void handle();
    public abstract byte[] toBytes();
    public abstract void parse(byte[] bytes);
    public abstract MsgType getMsgType();
}
