/**
 * Project Name: tank_practice
 * File Name: TankMsgEncoder
 * Package Name: com.hmz.tank.net
 * Date: 2020/8/10 20:11
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class TankJoinMsgEncoder extends MessageToByteEncoder<TankJoinMsg> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, TankJoinMsg msg, ByteBuf buf) throws Exception {
//        buf.writeInt(msg.x);
//        buf.writeInt(msg.y);
//        buf.writeBytes(msg.toBytes());
        buf.writeInt(msg.getMsgType().ordinal());
        byte[] bytes = msg.toBytes();
        buf.writeInt(bytes.length);
        buf.writeBytes(bytes);
    }

}
