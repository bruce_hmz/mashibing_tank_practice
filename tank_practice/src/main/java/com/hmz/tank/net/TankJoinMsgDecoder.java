/**
 * Project Name: tank_practice
 * File Name: TankMsgDecoder
 * Package Name: com.hmz.tank.net
 * Date: 2020/8/10 20:15
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

import com.hmz.tank.Dir;
import com.hmz.tank.Group;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.MessageSizeEstimator;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
import java.util.UUID;

public class TankJoinMsgDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf buf, List<Object> list) throws Exception {
        if (buf.readableBytes() < 8) {
            return ;
        }
//        msg.x = buf.readInt();
//        msg.y = buf.readInt();
//        msg.dir = Dir.values()[buf.readInt()];
//        msg.moving = buf.readBoolean();
//        msg.group = Group.values()[buf.readInt()];
//        msg.id = new UUID(buf.readLong(), buf.readLong());
        buf.markReaderIndex();
        MsgType msgType = MsgType.values()[buf.readInt()];
        int length = buf.readInt();

        byte[] bytes = new byte[length];
        buf.readBytes(bytes);

        switch(msgType) {
            case TankJoin:
                TankJoinMsg msg = new TankJoinMsg();
                msg.parse(bytes);
                list.add(msg);
                break;
            default:
                break;
        }
    }
}
