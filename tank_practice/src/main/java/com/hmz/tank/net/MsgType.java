/**
 * Project Name: tank_practice
 * File Name: MsgType
 * Package Name: com.hmz.tank.net
 * Date: 2020/8/17 20:10
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank.net;

public enum MsgType {
    TankJoin, TankDirChanged, TankStop, TankStartMoving, BulletNew, TankDie
}
