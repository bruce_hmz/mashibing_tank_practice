/**
 * Project Name: tank_practice
 * File Name: Tank
 * Package Name: com.hmz.tank
 * Date: 2020/7/26 13:20
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank;

import com.hmz.tank.net.TankJoinMsg;

import java.awt.*;
import java.util.Random;
import java.util.UUID;

public class Tank {
    private int x,y;
    private Dir dir = Dir.DOWN;
    private static final int SPEED = PropertyMgr.getIntValue("tankSpeed");
    public static int WIDTH = ResourceMgr.goodTankL.getWidth();
    public static int HEIGHT = ResourceMgr.goodTankL.getHeight();
    Rectangle rec = new Rectangle();
    private UUID id = UUID.randomUUID();

    private boolean moving = false;
    private boolean living = true;
    private TankFrame tf;
    private Random random = new Random();
    private Group group;

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public Tank(int x, int y, Dir dir,Group group,TankFrame tf) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.group = group;
        this.tf = tf;

        // init rec
        rec.x = x;
        rec.y = y;
        rec.height = HEIGHT;
        rec.width = WIDTH;
    }

    public Tank(TankJoinMsg msg) {
        x = msg.x;
        y = msg.y;
        dir = msg.dir;
        moving = msg.moving;
        group = msg.group;
        id = msg.id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Dir getDir() {
        return dir;
    }

    public void setDir(Dir dir) {
        this.dir = dir;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void paint(Graphics g) {
        if (!living) {
            tf.tanks.remove(this);
        }

        //uuid on head
        Color c = g.getColor();
        g.setColor(Color.YELLOW);
        g.drawString(id.toString(), this.x, this.y - 10);
        g.setColor(c);

        switch (dir) {
            case LEFT:
                g.drawImage(this.group == Group.GOOD ? ResourceMgr.goodTankL : ResourceMgr.badTankL,x,y,null);
                break;
            case RIGHT:
                g.drawImage(this.group == Group.GOOD ? ResourceMgr.goodTankR : ResourceMgr.badTankR,x,y,null);
                break;
            case UP:
                g.drawImage(this.group == Group.GOOD ? ResourceMgr.goodTankU : ResourceMgr.badTankU,x,y,null);
                break;
            case DOWN:
                g.drawImage(this.group == Group.GOOD ? ResourceMgr.goodTankD : ResourceMgr.badTankD,x,y,null);
                break;
        }
        move();
    }

    private void move() {
        if (!moving) {
            return;
        }
        switch (dir) {
            case UP:
                y -= SPEED;
                break;
            case DOWN:
                y += SPEED;
                break;
            case LEFT:
                x -= SPEED;
                break;
            case RIGHT:
                x += SPEED;
        }
        // 坏坦克随机发射子弹
        if (this.group == Group.BAD && random.nextInt(100) > 95 ) {
            this.fire();
        }
        // 换坦克随机方向移动
        if (this.group == Group.BAD && random.nextInt(100) > 95 ) {
             dir = Dir.values()[random.nextInt(4)];
        }
        // 坦克边界检测
        boundaryCheck();

        // update rec
        rec.x = x;
        rec.y = y;
    }

    private void boundaryCheck() {
        if (x <2) {
            x = 2;
        }
        if (y < 28) {
            y = 28;
        }
        if (x > (TankFrame.GAME_WIDTH - WIDTH - 2)) {
            x = TankFrame.GAME_WIDTH - WIDTH - 2;
        }
        if (y > (TankFrame.GAME_HEIGHT - HEIGHT - 2)) {
            y = TankFrame.GAME_HEIGHT - HEIGHT - 2;
        }
    }

    public void fire() {
//        tf.bullet = new Bullet(x,y,dir);
        int bx = x + Tank.WIDTH/2 - Bullet.WIDTH/2;
        int by = y + Tank.HEIGHT/2 - Bullet.HEIGHT/2;
        tf.bullets.add( new Bullet(bx,by,dir,this.group,this.tf));

    }

    public void die() {
        this.living = false;
    }
}
