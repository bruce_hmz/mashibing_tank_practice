/**
 * Project Name: tank_practice
 * File Name: PropertyMgr
 * Package Name: com.hmz.tank
 * Date: 2020/8/2 8:54
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank;

import java.io.IOException;
import java.util.Properties;

public class PropertyMgr {

    static Properties props = new Properties();

    static {
        try {
            props.load(PropertyMgr.class.getClassLoader().getResourceAsStream("config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getStringValue(String key) {
        if (props == null) {
            return null;
        }
        return props.getProperty(key);
    }

    public static int getIntValue(String key) {
        int num = Integer.parseInt(getStringValue(key));
        return num;
    }
}
