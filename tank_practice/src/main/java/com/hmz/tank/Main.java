/**
 * Project Name: tank_practice
 * File Name: Main
 * Package Name: com.hmz.tank
 * Date: 2020/7/26 11:42
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank;

import com.hmz.tank.net.Client;

public class Main {
    public static void main(String[] args) {

        TankFrame tf = TankFrame.INSTANCE;
        tf.setVisible(true);
        new Thread(()->{
            while(true) {
                try {
                    Thread.sleep(50);
                    tf.repaint();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

//        Client c = new Client();
//        c.connect();
        Client.INSTANCE.connect();
    }
}
