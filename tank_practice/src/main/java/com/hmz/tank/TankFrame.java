/**
 * Project Name: tank_practice
 * File Name: TankFrame
 * Package Name: com.hmz.tank
 * Date: 2020/7/26 11:41
 * Copyright (c) 2020,All Rights Reserved.
 */
package com.hmz.tank;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;
import java.util.List;

public class TankFrame extends Frame {

    public static final TankFrame INSTANCE = new TankFrame();
//    static final int GAME_WIDTH = PropertyMgr.getIntValue("gameWidth"),GAME_HEIGHT =PropertyMgr.getIntValue("gameHeight");
    static final int GAME_WIDTH = 1080, GAME_HEIGHT = 960;


    Random r = new Random();
    Tank myTank = new Tank(r.nextInt(GAME_WIDTH), r.nextInt(GAME_HEIGHT), Dir.DOWN, Group.GOOD, this);
    List<Bullet> bullets = new ArrayList<Bullet>();
//    List<Tank> tanks = new ArrayList<Tank>();
    Map<UUID,Tank> tanks = new HashMap<>();
    List<Explode> explodes = new ArrayList<Explode>();

    public void addTank(Tank t) {
//        for (int i = 0; i < tanks.size(); i++) {
//             if(t.getId().equals(tanks.get(i).getId())) {
//                 return;
//             }
//        }
        tanks.put(t.getId(),t);
    }

    public Tank findByUUID(UUID id) {
//        for(int i=0; i<tanks.size(); i++) {
//            if(id.equals(tanks.get(i).getId())) {
//                return tanks.get(i);
//            }
//        }
        return tanks.get(id);
    }


    /**
     * 构造方法初始化窗口
     */
    private TankFrame(){
        setTitle("tank war");
        setResizable(false);
        setSize(GAME_WIDTH,GAME_HEIGHT);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        addKeyListener(new MyKeyListener());
    }

    Image offScreenImage = null;
    @Override
    public void update(Graphics g) {
        if(offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.BLACK);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        paint(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);
    }

    @Override
    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.WHITE);
        g.drawString("子弹的数量:" + bullets.size(), 10, 60);
        g.drawString("敌人的数量:" + tanks.size(), 10, 80);
        g.drawString("爆炸的数量:" + explodes.size(), 10, 100);

        g.setColor(c);
        myTank.paint(g);
//        bullet.paint(g);
        // 画子弹
        for (int i = 0; i < bullets.size(); i++) {
            bullets.get(i).paint(g);
        }
        //画地方坦克
//        for (int i = 0; i < tanks.size(); i++) {
//             tanks.get(i).paint(g);
//        }
        tanks.values().stream().forEach((e) -> e.paint(g));

        // 画爆炸
        for (int i = 0; i < explodes.size(); i++) {
             explodes.get(i).paint(g);
        }
        /**
         * 判断每个子弹和每个坦克的碰撞
         */
        for (int i = 0; i < bullets.size(); i++) {
            for (int j = 0; j < tanks.size(); j++) {
                bullets.get(i).collideWith(tanks.get(j));
            }
        }
    }

    class MyKeyListener extends KeyAdapter {

        boolean bL = false;
        boolean bU = false;
        boolean bR = false;
        boolean bD = false;

        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    bL = true;
                    break;
                case KeyEvent.VK_UP:
                    bU = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    bR = true;
                    break;
                case KeyEvent.VK_DOWN:
                    bD = true;
                    break;
                case KeyEvent.VK_CONTROL:
                    myTank.fire();
                    break;
                default:
                    break;
            }
            setMainTankDir();
        }

        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    bL = false;
                    break;
                case KeyEvent.VK_UP:
                    bU = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    bR = false;
                    break;
                case KeyEvent.VK_DOWN:
                    bD = false;
                    break;
                default:
                    break;
            }
            setMainTankDir();
        }

        private void setMainTankDir() {
            //如果4个键都没又按 设置moving为false
            if(!bD && !bU && !bL && !bR){
                myTank.setMoving(false);
            } else {
                myTank.setMoving(true);
                if (bD) {
                    myTank.setDir(Dir.DOWN);
                }
                if (bU) {
                    myTank.setDir(Dir.UP);
                }
                if (bL) {
                    myTank.setDir(Dir.LEFT);
                }
                if (bR) {
                    myTank.setDir(Dir.RIGHT);
                }
            }
        }
    }
    public Tank getMainTank() {
        // TODO Auto-generated method stub
        return this.myTank;
    }
}
